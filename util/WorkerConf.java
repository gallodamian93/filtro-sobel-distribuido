package util;

public class WorkerConf {
	String ip;
	int port;
	
	
	//GETTERS & SETTERS
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	
	//CONSTRUCTOR
	public WorkerConf(String ip, int port) {
		super();
		this.ip = ip;
		this.port = port;
	}
	
}
