package util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.imageio.ImageIO;

public class Message implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum Action{applyFilter, filterApplied, error};
	Action action;
	byte[] body;
	//Object body;
	
	public Message(Action action, BufferedImage body) throws IOException{
		this.action = action;
		this.body = this.toByteArray(body);
		
	}
	
	private byte[] toByteArray(BufferedImage originalImage) throws IOException{
		byte[] imageInByte;
		// convert BufferedImage to byte array
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "jpg", baos);
		baos.flush();
		imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
	}
	
	
	public void setAction(Action action){
		this.action = action;
	}
	
	public BufferedImage getBody() throws IOException{
		return this.toBufferedImage(this.body);
	}
	
	private BufferedImage toBufferedImage(byte[] byteArray) throws IOException{
		InputStream in = new ByteArrayInputStream(byteArray);
		BufferedImage bImageFromConvert = ImageIO.read(in);
		return bImageFromConvert;
	}
	
	public Action getAction(){
		return this.action;
	}
	
	public String toString(){
		return "Action = " + this.action.toString();
	}
}


