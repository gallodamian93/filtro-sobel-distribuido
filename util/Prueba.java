package util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class Prueba {

	public static void main(String[] args) {
		try {

			byte[] imageInByte;
			BufferedImage originalImage = ImageIO.read(new File(
					"imagen.jpg"));

			// convert BufferedImage to byte array
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(originalImage, "jpg", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
			String str = new String(imageInByte, "UTF-8");
			System.out.println(str);
			// convert byte array back to BufferedImage
			InputStream in = new ByteArrayInputStream(imageInByte);
			BufferedImage bImageFromConvert = ImageIO.read(in);

			ImageIO.write(bImageFromConvert, "jpg", new File(
					"new-imagen.jpg"));

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

}
