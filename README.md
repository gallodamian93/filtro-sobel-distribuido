## Sobre el proyecto

El operador de Sobel es una máscara que, aplicada a una imagen, permite detectar
(resaltar) bordes. Este operador es una operación matemática que, aplicada a cada
pixel y teniendo en cuenta los pixeles que lo rodean, obtiene un nuevo valor (color)
para ese pixel. Aplicando la operación a cada pixel, se obtiene una nueva imagen que
resalta los bordes.

Este programa toma una imágen, la divide en n pedazos y asigna un pedazo para ser procesado a cada worker. Se utiliza RMI para la comunicación entre el servidor y los workers.

## Instrucciones

1. Poner una imagen (nombre "imagen.jpg") en la carpeta raíz del proyecto
2. Correr Worker.java (tiene una rutina que levanta 7 workers, 1 por puerto)
3. Correr Server.java 

La imagen sale como "unida.jpg" en la carpeta raíz del proyecto