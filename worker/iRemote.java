package worker;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface iRemote extends Remote{
	//float getTemperatura() throws RemoteException;
	Object apply(Object message) throws RemoteException, IOException;
}
