package worker;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import server.Server;
import util.Sobel;

public class Worker{
	Sobel sobel;
	int port;
	int publicPort;
	
	public Worker(int port, int publicPort){
		this.port = port;
		this.publicPort = publicPort;
		this.sobel = new Sobel();
	}
	public void publicar(){
		try {
			Registry myReg = LocateRegistry.createRegistry(port);
			
			iRemote stub = (iRemote) UnicastRemoteObject.exportObject(sobel,publicPort);
			
			myReg.rebind("sobel", stub);
			
			//el servidor ahora puede recibir peticiones
			System.out.println("Worker listo: " + port);
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws IOException {
		for(int i=0;i<7;i++){
			new Worker(10000+i, 8000+i).publicar();
		}
	}
}
