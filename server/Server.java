package server;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import util.Message;
import util.Message.Action;
import util.WorkerConf;
import worker.iRemote;

public class Server{
	ArrayList<WorkerConf> workers = new ArrayList<WorkerConf>();
	boolean finalizo = false;
	int totalImagenes;
	int imagenesTerminadas = 0;
	BufferedImage imagenes[];
	BufferedImage bordes[];
	int heightTotal;
	int widthTotal;
	static int cantFilas = 4; //debe ser par; = cantFilas
	
	public Server(){
		//carga rapida de workers, para implementacion se saca de aca y se carga a mano
		int port = 9999;
		for (int i=0;i<7;i++){
			WorkerConf w = new WorkerConf("127.0.0.1", port+1);
			workers.add(w);
		}
	}
	
	public void entregar(BufferedImage chunk, boolean esBorde, int pos){
		synchronized (this) {
			if (esBorde) {
				this.bordes[pos] = chunk;
			} else {
				this.imagenes[pos] = chunk;
			}
			imagenesTerminadas++;
			if(Integer.compare(totalImagenes, imagenesTerminadas) == 0){
				this.finalizo = true;
			}
		}
		
	}
	/*
	public Object send(String ip, int port, Object message){
		try {
			Registry workerRegistry = LocateRegistry.getRegistry(ip, port);
			iRemote remote = (iRemote) workerRegistry.lookup("sobel");
			Message respuesta = (Message) remote.apply(message);
			return respuesta;
		} catch (Exception e) {
			return null;
		}
	}*/
	
	public BufferedImage join(){
        BufferedImage concatImage = new BufferedImage(this.widthTotal, this.heightTotal, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = concatImage.createGraphics();
        int heightChunk = (heightTotal/cantFilas);
        //int offset = 0;   
        for (int i = 0; i < this.cantFilas; i++) {
        	//drawImage primeros 4 parametros: donde quiero escribir segundos 4 parametros: la imagen de la que saco data
			g2d.drawImage(this.imagenes[i], 0, i*heightChunk, widthTotal, heightChunk, null);
			//System.out.println("Puse bloque: " + (i*heightChunk) + "px");
			
			
			if(i < this.cantFilas -1){
				//(i+1)*heightChunk
				//g2d.drawImage(this.bordes[0], 0, 172, widthTotal, 177, 0, 1, widthTotal, 6, null);
				/*
				int x1 = 0;
				int y1 = ((i+1)*heightChunk)-2;
				int x2 = widthTotal;
				int y2 = ((i+1)*heightChunk)+3;
				g2d.drawImage(this.bordes[i], x1, y1, x2, y2, 0, 1, widthTotal, 6, null);
				
				System.out.println("Puse borde: ");
				System.out.println("border["+i+"]");
				System.out.println("x1="+x1+"  y1="+y1+"  x2="+x2+"  y2="+y2);*/
				//System.out.println("x1="+0+"  y1="+1+"  x2="+widthTotal+"  y2="+6);
			}
			
			//offset = 1;
			/*
			g2d.drawImage(this.bordes[0], 0, 172, widthTotal, 177, 0, 1, widthTotal, 6, null);
			g2d.drawImage(this.bordes[1], 0, 346, widthTotal, 351, 0, 1, widthTotal, 6, null);
			g2d.drawImage(this.bordes[2], 0, 520, widthTotal, 525, 0, 1, widthTotal, 6, null);
			*/
		}
        //esto es raro, si lo meto en una iteracion no funciona
		g2d.drawImage(this.bordes[0], 0, heightChunk*1-2, widthTotal, heightChunk*1+3, 0, 1, widthTotal, 6, null);
		g2d.drawImage(this.bordes[1], 0, heightChunk*2-2, widthTotal, heightChunk*2+3, 0, 1, widthTotal, 6, null);
		g2d.drawImage(this.bordes[2], 0, heightChunk*3-2, widthTotal, heightChunk*3+3, 0, 1, widthTotal, 6, null);
        /*
        for (int i = 0; i < this.cantFilas -1; i++){
        	//212 y 213
        	g2d.drawImage(this.bordes[i], 0, (i+1)*heightChunk-1, widthTotal, (i+1)*heightChunk, 0, 1, widthTotal, 3, null);
        }*/
        //g2d.drawImage(this.bordes[0], 0, 50, widthTotal, 52, 0, 1, widthTotal-1, 3, null);
        g2d.dispose();
        /*
        Graphics2D g2d = concatImage.createGraphics();
        int columnaOffset = 0;
        for(int columna = 0; columna < cantColumnas; columna++){
        	int filaOffset = 0;
        	for(int fila = 0; fila < cantColumnas; fila++){
        		g2d.drawImage(this.imagenes[fila+(columna*cantColumnas)], filaOffset, columnaOffset, null);
        		filaOffset+= this.widthTotal/cantColumnas;
        	}
        	columnaOffset+= this.heightTotal/cantColumnas;
        }
        g2d.dispose();
        */
        /*
        for(int j = 0; j < this.imagenes.length; j++) {
            g2d.drawImage(this.imagenes[j], 0, heightCurr, null);
            heightCurr += this.imagenes[j].getHeight();
        }
        g2d.dispose();
        */
        
		return concatImage;
	}
	
	public void splitImage(String ruta) throws IOException{
		File file = new File(ruta);
        FileInputStream fis = new FileInputStream(file);
        BufferedImage image = ImageIO.read(fis);

        int rows = cantFilas;
        int cols = 1;
        int chunks = rows * cols;

        this.widthTotal = image.getWidth();
        this.heightTotal = image.getHeight();
        
        int chunkWidth = image.getWidth() / cols; 
        int chunkHeight = image.getHeight() / rows;
        int count = 0;
        
        //imagenes
        BufferedImage imgs[] = new BufferedImage[chunks];
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                
                imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                
                Graphics2D gr = imgs[count++].createGraphics();
                gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x, chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
                gr.dispose();
            }
        }
        this.imagenes = imgs;
        
        //bordes
        Graphics2D gr2 = null;
        int cantBordes = chunks-1;
        BufferedImage myBordes[] = new BufferedImage[cantBordes];
        //int myOffset = 500;
        count = 0;
        for(int i=0; i<cantBordes;i++){
        	myBordes[count] = new BufferedImage(chunkWidth, 8, image.getType());
        	
        	gr2 = myBordes[count++].createGraphics();
        	gr2.drawImage(image, 0, 0, chunkWidth, 7, 0, chunkHeight*(i+1)-3, chunkWidth, chunkHeight*(i+1)+4, null);
        }
        gr2.dispose();
        this.bordes = myBordes;
        
        /*
        System.out.println("SplitImage: Done!");

        for (int i = 0; i < imgs.length; i++) {
            ImageIO.write(imgs[i], "jpg", new File("img" + i + ".jpg"));
        }*/
        this.totalImagenes = this.imagenes.length + this.bordes.length;
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		Server s = new Server();
		s.splitImage("imagen.jpg");
		//File file = new File("img01.jpg");
        //FileInputStream fis = new FileInputStream(file);
        //BufferedImage image = ImageIO.read(fis);
		
		for (int i=0;i<4;i++) {
			Thread svmThread = new Thread(new ServerManager(s.workers.get(i), s.imagenes[i], false, s, i));
			svmThread.start();
		}
		for (int i=0;i<3;i++) {
			Thread svmThread = new Thread(new ServerManager(s.workers.get(i), s.imagenes[i], true, s, i));
			svmThread.start();
		}
		
		while (true){
			if(s.finalizo){
				break;
			}
			Thread.sleep(100);
		}
		
		ImageIO.write(s.join(), "jpg", new File("unida.jpg"));
		System.out.println("FINALIZADO!");
		/*
		int i =0;
		for (BufferedImage chunk : s.imagenes) {
	        Message msg = new Message(Action.applyFilter, s.imagenes[i]);
			Message respuesta = (Message)s.send("127.0.0.1", 9000, msg);
			s.imagenes[i] = respuesta.getBody();
			//ImageIO.write(respuesta.getBody(), "jpg", new File("img"+i+".jpg"));
			
			if(i < s.bordes.length){
		        Message msg2 = new Message(Action.applyFilter, s.bordes[i]);
				Message respuesta2 = (Message)s.send("127.0.0.1", 9000, msg2);
				s.bordes[i] = respuesta2.getBody();
				//ImageIO.write(respuesta2.getBody(), "jpg", new File("borde"+i+".jpg"));
			}
			//System.out.println(respuesta.toString());
			i++;
		}
		ImageIO.write(s.join(), "jpg", new File("unida.jpg"));
		System.out.println("FINALIZADO!");
		*/
	}
}
