package server;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import util.WorkerConf;
import util.Message;
import util.Message.Action;
import worker.iRemote;

public class ServerManager implements Runnable{
	BufferedImage image;
	WorkerConf workerConf;
	boolean esBorde;
	Server server;
	int pos;
	
	public ServerManager(WorkerConf worker, BufferedImage image, boolean esBorde, Server server, int pos) {
		this.workerConf = worker;
		this.image = image;
		this.esBorde = esBorde;
		this.server = server;
		this.pos = pos;
	}
	
	@Override
	public void run() {
		Message msg;
		Message respuesta;
		try {
			msg = new Message(Action.applyFilter, image);
			respuesta = (Message)send(workerConf.getIp(), workerConf.getPort(), msg);
			server.entregar(respuesta.getBody(), esBorde, pos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			respuesta = null;
		}
		
		
		/*
		int i =0;
		for (BufferedImage chunk : s.imagenes) {
	        Message msg = new Message(Action.applyFilter, s.imagenes[i]);
			Message respuesta = (Message)s.send("127.0.0.1", 9000, msg);
			s.imagenes[i] = respuesta.getBody();
			//ImageIO.write(respuesta.getBody(), "jpg", new File("img"+i+".jpg"));
			
			if(i < s.bordes.length){
		        Message msg2 = new Message(Action.applyFilter, s.bordes[i]);
				Message respuesta2 = (Message)s.send("127.0.0.1", 9000, msg2);
				s.bordes[i] = respuesta2.getBody();
				//ImageIO.write(respuesta2.getBody(), "jpg", new File("borde"+i+".jpg"));
			}
			//System.out.println(respuesta.toString());
			i++;
		}
		ImageIO.write(s.join(), "jpg", new File("unida.jpg"));
		System.out.println("FINALIZADO!");
		*/
	}
	
	public Object send(String ip, int port, Object message){
		try {
			Registry workerRegistry = LocateRegistry.getRegistry(ip, port);
			iRemote remote = (iRemote) workerRegistry.lookup("sobel");
			Message respuesta = (Message) remote.apply(message);
			return respuesta;
		} catch (Exception e) {
			return null;
		}
	}

}
